'use strict';


const CreateNewUser = function () {

    let firstName = prompt('Enter your name', '');
    while (firstName === null || !firstName.match(/^[a-zA-Zа-яА-ЯёЁЇїІіЄєҐґ"]+$/)
        ) {
        alert("It's not a name!");
        firstName = prompt('Enter your name', '');
    }
    let lastName = prompt('Enter your last name', '');

    while (lastName === null || !lastName.match(/^[a-zA-Zа-яА-ЯёЁЇїІіЄєҐґ"]+$/)
        ) {
        alert("It's not a last name!");
        lastName = prompt('Enter your last name', '');
    }

    this.setFirstName = (value) => firstName = value;

    this.setLastName = (value) => lastName = value;

    this.getLogin = () => `${firstName[0].toLowerCase()}${lastName.toLowerCase()}`;


};

const newUser = new CreateNewUser();

console.log(newUser.getLogin());//вывод метода

newUser.setFirstName('Иван');//смена имени
newUser.setLastName('Попыхайло');//смена фамилии
//
console.log(newUser.getLogin());//вывод метода

console.log(newUser);

newUser.setLastName('Смирнов');//смена фамилии
console.log(newUser.getLogin());//вывод метода

