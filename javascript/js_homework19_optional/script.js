'use strict';


const user = {
    number: 1,
    string: "2",
    boolean: true,
    array: [1, "2"],
    date: new Date(),
    func: function (a, b) { return a + b; },
    regex: new RegExp(/aaa/i),
    subObj:
        {
            number: 2,
            string: "3",
            boolean: true,
            array: [4, "5"],
            date: new Date(),
            func: function (q) { return q + 1; },
            regex: new RegExp(/aaa/i)
        }
};
// расширяет объект «from» свойствами с «to». Если 'to' равно нулю, возвращается глубокий клон 'from'
// modified from: https://stackoverflow.com/questions/122102/what-is-the-most-efficient-way-to-deep-clone-an-object-in-javascript


function deepExtend(from, to) {
    if (from == null || typeof from !== 'object') {
        return from;
    }
    if (from.constructor !== Object && from.constructor !== Array) {
        return from;
    }
    if (from.constructor === Date ||
        from.constructor === RegExp ||
        from.constructor === Function ||
        from.constructor === String ||
        from.constructor === Number ||
        from.constructor === Boolean) {
        return new from.constructor(from);
    }

    to = to || new from.constructor();

    for (let name in from) {
        if (from.hasOwnProperty(name)) {
            to[name] = typeof to[name] === 'undefined'
                ? deepExtend(from[name], null)
                : to[name];
        }
    }

    return to;
}


const newUser = deepExtend(user);

// изменяем свойства объекта user
user.number = 5;
user.string = "5";
user.array = [1, 2, true, { name: "test2" }];
user.boolean = false;
user.date = new Date('2018-5-2');
user.func = function (a, b, c) { return a + b + c };
user.regex = new RegExp(/[a]{5}/i);

user.subObj.number = 5;
user.subObj.string = "5";
user.subObj.array = [1, 2, true, { name: "test2" }];
user.subObj.boolean = false;
user.subObj.date = new Date('2018-5-2');
user.subObj.func = function (a, b, c) { return a + b + c };
user.subObj.regex = new RegExp(/[a]{5}/i);

console.log(user);
console.log(newUser);

//клонирование либо через spreed Оператор
const clonedObj = { ...user };
console.log(clonedObj);

//клонирование через Метод Object.assign() копирует из исходных объектов в целевой объект только перечисляемые и собственные свойства
const copy = Object.assign({}, newUser);
console.log(copy);