'use strict';
const inputData = [{
    name: "Германия",
    language: "немецкий",
    cities: [
        {name: "Цюрих", population: 378884},
        {name: "Женева", population: 188634},
        {name: "Базель", population: 164937}
    ],
    capital: {

        name: "Берлин",
        population: 3375000,
        year: 1237,
        capital: {

            name: "Лондон",
            population: 8854000,
            year: 1896,
            capital: {

                name: "Цюрих",
                languages: ["немецкий", "французский", ['hello', 'world', null, [
                    {name: "Цюрих", population: 378884},
                    {name: "Женева", population: 188634},
                    {name: "Базель", population: 164937, crazy:[{name: "поселок \" Лукоморье\"", population: 37}, 'world', null]}
                ]], "итальянский"],
                population: 4455000,
                year: 2019,
                capital: {

                    name: "Москва",
                    population: 1000456,
                    year: 1042,
                },
                "crazy": 1000455,
            }
        }
    }
}, [['a', 'b'], ['baz', ['hello', 'world'],], {
    id: 2,
    name: 'bar'
}], 'Kharkiv', 'Odessa', {
    id: 2, name: 'bar'
}, 'Lviv', null];


function getTimer(timer, list) {
    let count = 10;

    let timerSet = setInterval(function () {
        timer.innerText = count;

        if (count === 0) {
            list.hidden = true;

            timer.hidden = true;

            clearInterval(timerSet);
        }

        count--;
    }, 1000);
}

function getSubList(item) {
    //цикл по объекту
    let objList = '';
    for (let prop in item) {
        //полная проверка на вложенный массив в объекте, чтобы не ругался webStorm
        if (Object.prototype.toString.call(item[prop]) === '[object Array]' && item.hasOwnProperty(prop)) {
            //рекурсивный вызов
            const childList = getList(item[prop]).join('');

            objList += `<li><ul>${childList}</ul></li>`;
        }
        //проверка на вложенный объект
        else if (typeof item[prop] === "object" && item.hasOwnProperty(prop)&& item[prop] !== null ) {
            //рекурсивный вызов
            const objChildList = getSubList(item[prop]);

            objList += `<li>${objChildList}</li>`;

        }
        //если не вложенный перебираем
        else if (item.hasOwnProperty(prop)) {
            objList += `<li>${item[prop]}</li>`;
        }
    }
    return `<ul>${objList}</ul>`;
}

function getList(arr) {
    //цикл по полученному массиву объектов и массивов
    return arr.map(item => {
        //проверка по массиву
        if (Array.isArray(item)) {
            //рекурсивный вызов
            const childList = getList(item).join('');

            return `<li><ul>${childList}</ul></li>`;
        }
        //проверка на объект
        else if (typeof item === "object" && item !== null) {
            //цикл по объекту
            let objList = '';
            for (let prop in item) {
                //проверка на вложенный объект
                if (typeof item[prop] === "object" && item.hasOwnProperty(prop)) {
                    const objChildList = getSubList(item[prop]);

                    objList += `<li>${objChildList}</li>`;
                }
                //перебираем объект
                else if (item.hasOwnProperty(prop)) {
                    objList += `<li>${item[prop]}</li>`;
                }
            }
            return `<li><ul>${objList}</ul></li>`;
            //если не объект и не массив возвращаем пункт списка
        } else {
            return `<li>${item}</li>`;
        }
    });
}

//функция получения списка
function showList(list) {
    let timer = document.getElementById("timer");

    const ul = document.createElement('ul');
    ul.className = 'list';
    ul.innerHTML = getList(list).join('');

    timer.after(ul);

    // getTimer(timer, ul);
}

showList(inputData);

