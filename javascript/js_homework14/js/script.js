'use strict';
$('.centered-content .content').not(":first").hide();
//click on the title
$(".tabs .tabs-title")
    .on('click', function () {
    //all titles - removeClass, and currentTarget - add .active
    $(".tabs-title ").removeClass("active").eq($(this).index()).addClass("active");
    //all subtitles - hide, and currentTarget - show
    $(".content").hide().eq($(this).index()).fadeIn();
    //the first - add .active
})
    .eq(0)
    .addClass("active");


// $('.tabs-title').on('click',(event)=> {
//     $('.tabs-title').each(function(elemIndex){
//     if (this === event.target){
//         //the element on which the event occurred add class'.active'
//         $(event.target).addClass('active');
//         // get the elem at the specified index "eq(elemIndex)" and add class'.active'
//         $('.content').eq(elemIndex).addClass('active');
//     }else{
//         // the remaining elements remove the class '.active'
//         $(this).removeClass('active');
//         $('.content').eq(elemIndex).removeClass('active');
//     }
// });
// });



