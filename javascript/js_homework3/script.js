'use strict';

//функция проверки на ввод, отсеивается всё, кроме строк-чисел и обычных чисел

function validNumber(n) {
    return (!isNaN(parseFloat(n)) && isFinite(n));
}


function getTwoNumbersAndOperation() {
    let firstNumber = +prompt('Введите первое число', '1');
    while (!validNumber(firstNumber)) {
        firstNumber = +prompt('Вы ввели не правильное значение!!! Введите число', '1');
    }

    let operation = prompt('Введите операцию с числами', '+');
    while (
        operation !== '+'
        && operation !== '-'
        && operation !== '*'
        && operation !== '/'
        ) {
        operation = prompt(`Вы ввели не правильное значение!!! Введите операции '+', '-', '*' или '/'`, `+`)
    }

    let secondNumber = +prompt('Введите второе число', '2');
    while (!validNumber(secondNumber)) {
        secondNumber = +prompt('Вы ввели не правильное значение!!! Введите число', '2');
    }
    return [firstNumber, secondNumber, operation];//передаем массив значений
}


function calc(arg) {//принимаем в качестве аргумента массив
    let number1 = arg[0];
    let number2 = arg[1];
    let operation = arg[2];


    let result;
    switch (operation) {
        case '+':
            result = number1 + number2;
            break;
        case '-':
            result = number1 - number2;
            break;
        case '*':
            result = number1 * number2;
            break;
        case '/':
            result = number1 / number2;
            break;
    }

    console.log(`${number1} ${operation} ${number2} = ${result}`);

    return result;
}
let twoNumbersAndOperation = getTwoNumbersAndOperation();

calc(twoNumbersAndOperation);




