'use strict';


const CreateNewUser = function () {
    //получение имени пользователя с проверкой на ввод и закрытым для доступа снаружи объекта
    let firstName = prompt('Enter your name', '');
    while (firstName === null || !firstName.match(/^[a-zA-Zа-яА-ЯёЁЇїІіЄєҐґ"]+$/)
        ) {
        alert("It's not a name!");
        firstName = prompt('Enter your name', '');
    }

    //получение фамилии пользователя с проверкой на ввод и закрытым для доступа снаружи объекта
    let lastName = prompt('Enter your last name', '');
    while (lastName === null || !lastName.match(/^[a-zA-Zа-яА-ЯёЁЇїІіЄєҐґ"]+$/)
        ) {
        alert("It's not a last name!");
        lastName = prompt('Enter your last name', '');
    }

    //метод для изменения имени пользователя
    this.setFirstName = (value = firstName) => firstName = value;

    //метод для изменения фамилии пользователя
    this.setLastName = (value = lastName) => lastName = value;

    //метод получения логина пользователя
    this.getLogin = () => `${firstName[0].toLowerCase()}${lastName.toLowerCase()}`;

    //получение даты рождения пользователя с проверкой на ввод
    this.birthday = prompt('Write your birthday dd.mm.yyyy: ', '');
    // Это регулярное выражение проверяет, является ли дата в формате(дд.мм.гггг) или(д.м.гггг).Високосный год тоже нужно проверять. Дата должна быть между 01.01.1900 и 31.12.2999.
    let regexp = /^(((0?[1-9]|[12]\d|3[01])\.(0[13578]|[13578]|1[02])\.((1[6-9]|[2-9]\d)\d{2}))|((0?[1-9]|[12]\d|30)\.(0[13456789]|[13456789]|1[012])\.((1[6-9]|[2-9]\d)\d{2}))|((0?[1-9]|1\d|2[0-8])\.0?2\.((1[6-9]|[2-9]\d)\d{2}))|(29\.0?2\.((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/;
    while (this.birthday === null || !this.birthday.match(regexp)) {
        alert('Incorrect date format -> \'dd.mm.yyyy\'  or -> \'d.m.yyyy\'  ');
        this.birthday = prompt('Write your birthday dd.mm.yyyy: ', '');
    }

    // получаем возраст пользователя - рассчитывается из текущей даты и дня рождения
    this.getAge = function () {
        //делаем массив из полученной даты
        let parseBirthday = this.birthday.split('.');
        //меняем местами значения полученной даты,записываем в переменную и привязываем к объекту
        this.birthdayDate = new Date(`${parseBirthday[2]}-${parseBirthday[1]}-${parseBirthday[0]}`);
        //сегодняшняя дата
        let today = new Date();
        //получаем сегодняшний год
        let todayYear = new Date().getFullYear();
        //вычисляем сколько лет пользователю
        let age = todayYear - this.birthdayDate.getFullYear();
        let month = today.getMonth() - this.birthdayDate.getMonth();
        //доп проверка на месяц
        if (month < 0 || (month === 0 && today.getDate() < this.birthdayDate.getDate())){
                    age--;
        }
        return age;
    };
    //метод получения пароля пользователя
    this.getPassword = () => `${firstName[0].toUpperCase()}${lastName.toLowerCase()}${this.birthdayDate.getFullYear()}`;
};

const newUser = new CreateNewUser();
// console.log(newUser.setFirstName('Petr'));
// console.log(newUser.setLastName('Tramp'));
// console.log(newUser.getLogin());

console.log(newUser);//вывод объекта
console.log(newUser.getAge());//вывод метода
console.log(newUser.getPassword());//вывод метода

