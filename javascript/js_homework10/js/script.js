'use strict';

const firstIcon = document.getElementById('first-icon');
const firstPass = document.getElementById('first-pass');

firstIcon.addEventListener('click', function () {
    if (firstPass.type === 'password') {
        firstPass.type = 'text';
        firstIcon.classList.replace('fa-eye-slash', 'fa-eye');
    } else {
        firstPass.type = 'password';
        firstIcon.classList.replace('fa-eye', 'fa-eye-slash');
    }
});

const secondIcon = document.getElementById('second-icon');
const secondPass = document.getElementById('second-pass');

secondIcon.addEventListener('click', function () {
    if (secondPass.type === 'password') {
        secondPass.type = 'text';
        secondIcon.classList.replace('fa-eye-slash', 'fa-eye');
    } else {
        secondPass.type = 'password';
        secondIcon.classList.replace('fa-eye', 'fa-eye-slash');
    }
});

const btn = document.getElementById('btn');
const errorMessage = document.getElementById('error');
errorMessage.style.color = 'red';

btn.addEventListener('click', () => {
    if (firstPass.value !== secondPass.value) {

        errorMessage.innerText = 'Нужно ввести одинаковые значения';

    }else if(firstPass.value === ''&& secondPass.value === ''){
        errorMessage.innerText = 'Значение пароля не должно быть пустым';
    }
    else {
        errorMessage.innerText = '';
        firstPass.value = '';
        secondPass.value = '';
        alert('You are welcome');

    }
    // clearTimeout(timeOut);
});


