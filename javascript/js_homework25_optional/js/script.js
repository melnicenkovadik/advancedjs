let prevButton = document.getElementsByClassName('prev')[0];
let nextButton = document.getElementsByClassName('next')[0];
let images = document.querySelector('.main-img');

function megaFunction(numSlide,butt){
  if (!numSlide.src) {
    butt.click();
    if (!numSlide.src) {
      butt.click();      
    }    
  }
}

prevButton.onclick = (() => {

  let last = images.lastChild;

  images.insertBefore(last, images.firstChild);

  megaFunction(last,prevButton);

  images.classList.add('back');

  setTimeout(() => {
    images.classList.remove('back');

  }, 300)

});
nextButton.click();

nextButton.onclick = (() => {

  let first = images.firstChild;

  images.appendChild(first);

  megaFunction(first,nextButton);
  
  images.classList.add('forward');

  setTimeout(() => {
    images.classList.remove('forward');
  }, 300)
});