'use strict';
function validNumber(n) {
    return (!isNaN(parseFloat(n)) && isFinite(n));
}
function getDisciplineMark() {
    let mark = +prompt(('Enter mark'), '8');
    while (!validNumber(mark)) {
        mark = +prompt('You entered the wrong value !!! Insert the number', '5');
    }
    return mark;
}


const student = {
    name:'',
    lastName:'',
    table: {}
};

student.name = prompt('Enter your name');
student.lastName = prompt('Enter your last name');

let discipline,
    disciplineMark,
    sumGoodMark = 0,
    countBadMarks = 0;

while ((discipline = prompt('enter discipline name'))!== null) {
    disciplineMark = getDisciplineMark();
    student.table[discipline] = disciplineMark;
    sumGoodMark += disciplineMark;
    if (disciplineMark < 4) {
        countBadMarks++;
    }
}
console.log(student);

const averageMark = (sumGoodMark / Object.keys(student.table).length)
    .toFixed(2);
console.log('Bad marks: ', countBadMarks);
if (countBadMarks === 0) {
    alert('Student will transferred to the next course')
}

console.log('Average of marks: ', averageMark);
if (averageMark > 7) {
    alert('Student will have a scholarship.');
}
