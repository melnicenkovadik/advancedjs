'use strict';

    document.addEventListener('keydown', function(event){
        const buttons = document.querySelectorAll('.btn');

        buttons.forEach(function(item){
            item.classList.remove('blue');
        });

        //key – значение  символ('Enter' не меняет значения от раскладки)
        if (event.key === 'Enter') {
            buttons[0].classList.add('blue');
        }
        //code – «физический код клавиши»
        if (event.code === 'KeyS') {
            buttons[1].classList.add('blue');
        }
        if (event.code === 'KeyE') {
             buttons[2].classList.add('blue');
        }
        if (event.code === 'KeyO') {
             buttons[3].classList.add('blue');
        }
        if (event.code === 'KeyN') {
             buttons[4].classList.add('blue');
        }
        if (event.code === 'KeyL') {
             buttons[5].classList.add('blue');
        }
        if (event.code === 'KeyZ') {
             buttons[6].classList.add('blue');
        }
    });
