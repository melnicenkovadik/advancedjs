'use strict';
const inputData = ['hello', 'world',['a', 'b'], 'Kiev', [['a', 'b'], ['baz', {id: ['hello', 'world'], name: 'bar'}], {id: 2, name: 'bar'}], 'Kharkiv', 'Odessa', {id: 2, name: 'bar'}, 'Lviv', null];

function getTimer(timer, list) {
    let count = 10;

    let timerSet = setInterval(function () {
        timer.innerText = count;

        if (count === 0) {
            list.hidden = true;

            timer.hidden = true;

            clearInterval(timerSet);
        }

        count--;
    }, 1000);
}


function getList(arr) {
    return arr.map(item => {
        if (Array.isArray(item)) {
            const childList = getList(item).join('');

            return `<li><ul>${childList}</ul></li>`;
        } else if (typeof item === "object" && item !== null) {
            let objList = '';

            for (let prop in item) {
                if (item.hasOwnProperty(prop)) {
                    objList += `<li>${item[prop]}</li>`;
                }
            }

            return `<li><ul>${objList}</ul></li>`;
        } else {
            return `<li>${item}</li>`;
        }
    });
}


function showList(list) {
    let timer = document.getElementById("timer");

    const ul = document.createElement('ul');
    ul.className = 'list';
    ul.innerHTML = getList(list).join('');

    timer.after(ul);

    getTimer(timer, ul);
}

showList(inputData);

